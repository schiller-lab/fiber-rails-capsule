# Hysteresis in spreading and retraction of liquid droplets on parallel fiber rails

##### Fang Wang and Ulf D. Schiller
##### [Schiller Research Group](https://cecas.clemson.edu/compmat/), Department of Materials Science and Engineering, Clemson University

## Overview

This capsule contains the data analysis of lattice Boltzmann simulations of spreading and retraction of liquid droplets on two parallel cylindrical fibers. The analysis includes the time evolution of the wetting length, surface energy, and excess Laplace pressure. A complete morphology diagram is constructed and the regions of the diagram are classified using support vector classification. 

### License

The code contained in this capsule is released under the terms of the BSD-3-Clause license. The data contained in this capsule is released under the Creative Commons Attribution-ShareAlike (CC BY-SA) license. Please refer to the `LICENSE` files for specific provisions of the licenses.

## Quick start

The complete analysis is described in the Jupyter notebook `fiber-rails.ipynb`. This notebook is executed during a "Reproducible Run" and generates the figures and tables in the results folder. The notebook can also be launched in a Cloud Workstation for further exploration.

The directories in this capsule are organized as follows:

* code/ contains the notebook, execution script, and nbconvert templates
* data/ contains the post-processed data from lattice Boltzmann simulations
* data/figures/ contains figures from external tools (e.g., Paraview) that are imported in the notebook

### Dependencies

The following Python packages are imported in the notebook:

- Jupyter
- NumPy
- SciPy
- Matplotlib
- Pillow
- pandas
- seaborn
- scikit-learn

These packages are provisioned through conda, see the `Dockerfile` for the versions used in this capsule. Note that Pillow requires Ghostscript to be installed for processing the included EPS graphics.

## Research abstract

Wetting and spreading of liquids on fibers occurs in many natural and artificial processes. Unlike on a planar substrate, a droplet attached to one or more fibers can assume several different shapes depending on geometrical parameters such as liquid volume and fiber size and distance. This paper presents lattice Boltzmann simulations of the morphology of liquid droplets on two parallel cylindrical fibers. We investigate the final shapes resulting from spreading of an initially spherical droplet deposited on the fibers and from retraction of an initial liquid column deposited between the fibers. We observe three possible equilibrium configurations: barrel-shaped droplet, droplet bridges, and liquid columns. We determine the complete morphology diagram for varying inter-fiber spacing and liquid volume and find a region of bistability that spans both the column regime and the droplet regime. We further present a simulation protocol that allows to probe the hysteresis of transitions between different shapes. The results provide insights into energies and forces associated with shape transformations of droplets on fibers that can be used to develop fiber-based materials and microfluidic systems for manipulation of liquids at small scale.

## Acknowledgments

The `nbconvert` templates are adapted from work by [Julius Schulz and Alexander Schlaich](https://github.com/schlaicha/jupyter-publication-scripts).
