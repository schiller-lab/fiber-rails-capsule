#!/bin/bash
set -e

ln -snf ../results ./fiber-rails_files

jupyter nbconvert --to latex \
    --ExecutePreprocessor.allow_errors=True \
    --ExecutePreprocessor.timeout=-1 \
    --FilesWriter.build_directory=. \
    --execute 'fiber-rails.ipynb'
